/* %Z% parse JPEG file */

#include	<stdio.h>
#include	<string.h>
#include	<stdarg.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<errno.h>
#include	<fcntl.h>
#include	<termios.h>

#define		FILENAME		80			/* max file name size */
#define		INTERVAL		10000000	/* report interval */

#define		MARKER			0xff		/* segment marker */

#define		STATE_NONE		0			/* no state (waiting for marker) */
#define		STATE_MARKER	1			/* found marker */
#define		STATE_LENHI		2			/* waiting for hi length byte */
#define		STATE_LENLO		3			/* waiting for lo length byte */
#define		STATE_SKIPPING	4			/* skipping a segment */
#define		STATE_IMAGE		5			/* in compressed image data */
#define		STATE_IMARKER	6			/* marker inside image */
#define		STATE_SOI		7			/* start of image */
#define		STATE_SOIM		8			/* start of image + marker */
#define		STATE_APP00		10			/* APP0 segment byte 0 */
#define		STATE_APP01		11			/* APP0 segment byte 1 */
#define		STATE_APP02		12			/* APP0 segment byte 2 */
#define		STATE_APP03		13			/* APP0 segment byte 3 */
#define		STATE_APP04		0xe0		/* APP0 segment byte 4 */
#define		STATE_APP10		20			/* APP1 segment byte 0 */
#define		STATE_APP11		21			/* APP1 segment byte 1 */
#define		STATE_APP12		22			/* APP1 segment byte 2 */
#define		STATE_APP13		23			/* APP1 segment byte 3 */
#define		STATE_APP14		0xe1		/* APP1 segment byte 4 */

#define		TRUE			1			/* boolean `true' */
#define		FALSE			0			/* boolean `false' */

#define		ARGCHAR			'-'			/* argument delimiter */
#define		PATHSEP			'/'			/* path separator */

#define		GETINT(s, x)	(sscanf(s, Int, x) == 1)
#define		MKBUF(t, n)		(t *) mkbuf(sizeof(t) * n)

static int		writefile(int length, int type);
static void		parse(FILE * ifp);
static void		putbyte(int fd, int value, int pos);
static void		setprog(char * name);
static void		error(const char * fmt, ...)
					__attribute__ ((format(printf, 1, 2), noreturn));
static void		whyerror(const char * fmt, ...)
					__attribute__ ((format(printf, 1, 2), noreturn));
static void *	mkbuf(int size);

static const char *	Usage   = "usage: %s [-f #] [-o file] [file]";
static const char *	Outfile = "found%04d.jpeg";
static const char *	Int     = "%i";
static const char *	Read    = "r";

static int			fnum;		/* file number */
static int			debugflag;	/* debug flag */
static char *		prog;		/* program name */

int
main(
	int			argc,	/* argument count */
	char **		argv,	/* argument vector */
	char **		envp)	/* environment pointer */
{
	char *		va;		/* value array */
	char *		nvp;	/* needed value pointer */
	char *		cvp;	/* current value pointer */
	char *		evp;	/* end of value pointers */
	FILE *		ifp;	/* input file pointer */

	setprog(*argv);

	va        = MKBUF(char, argc);
	nvp       = va;
	cvp       = va;
	evp       = va + argc;
	ifp       = stdin;
	debugflag = FALSE;

	while (--argc) {
		if (**++argv == ARGCHAR) {
			while (*++*argv) {
				switch (**argv) {
					case 'f':	/* start file number */
					case 'o':	/* output file name */
						if (nvp == evp) {
							error(Usage, prog);
						}

						*nvp++ = **argv;
						break;

					case 'D':	/* debug */
						debugflag = TRUE;
						break;

					default:
						error(Usage, prog);
				}
			}
		} else {
			if (nvp > cvp) {
				switch (*cvp++) {
					case 'f':	/* start file number */
						if (!GETINT(*argv, &fnum)) {
							error(Usage, prog);
						}
						break;

					case 'o':	/* output file name */
						Outfile = *argv;
						break;
				}
			} else {
				ifp = fopen(*argv, Read);

				if (ifp == NULL) {
					whyerror(*argv);
				}
			}
		}
	}

	if (nvp > cvp) {
		error(Usage, prog);
	}

	free(va);

	parse(ifp);

	return(EXIT_SUCCESS);
}

/* parse file */

static void
parse(
	FILE *	ifp)		/* input file pointer */
{
	int		state;		/* parse state */
	int		byte;		/* current byte */
	int		count;		/* byte count */
	int		start;		/* start byte */
	int		length;		/* segment length */
	int		image;		/* image flag */
	int		app;		/* APP0/APP1 flag */
	int		ofd;		/* output file descriptor */

	count = 0;
	app   = 0;
	ofd   = -1;
	image = FALSE;
	state = STATE_NONE;	/* no state */

	/* below initializations aren't strictly necessary */

	length = 0;
	start  = 0;
	
	for (;;) {
		byte = getc(ifp);

		if (byte == EOF) {
			if (ofd >= 0) {
				printf("truncated file!\n");
				close(ofd);
			}

			return;
		}

		if ((++count % INTERVAL) == 0) {
			printf("passing byte %d\n", count);
		}

		if (ofd >= 0) {
			putbyte(ofd, byte, count - start);
		}

		switch (state) {
			case STATE_NONE:	/* expect marker */
				if (byte == MARKER) {
					state = STATE_MARKER;
				}
				break;

			case STATE_SOI:
				state = (byte == 0xff) ? STATE_SOIM : STATE_NONE;
				break;

			case STATE_SOIM:
				switch (byte) {
					case 0xe0:
						app   = STATE_APP00;
						state = STATE_LENHI;
						break;

					case 0xe1:
						app   = STATE_APP10;
						state = STATE_LENHI;
						break;

					default:
						state = STATE_NONE;
						break;
				}
				break;

			case STATE_MARKER:
				switch (byte) {
					case 0xd8:
						state = STATE_SOI;
						break;

					case 0xd9:
						if (ofd >= 0) {
							printf("%d bytes\n",
								count - start);
							close(ofd);
							ofd   = -1;
							state = STATE_NONE;
						}
						break;

					case 0xda:
						state = STATE_LENHI;

						if (ofd >= 0) {
							image = TRUE;
						}
						break;
	
					case 0xff:
						break;

					case 0xc0:
					case 0xc1:
					case 0xc2:
					case 0xc3:
					case 0xc4:
					case 0xc5:
					case 0xc6:
					case 0xc7:
					case 0xc8:
					case 0xc9:
					case 0xca:
					case 0xcb:
					case 0xcc:
					case 0xcd:
					case 0xce:
					case 0xcf:
					case 0xdb:
					case 0xdc:
					case 0xdd:
					case 0xde:
					case 0xdf:
					case 0xe0:
					case 0xe1:
					case 0xe2:
					case 0xe3:
					case 0xe4:
					case 0xe5:
					case 0xe6:
					case 0xe7:
					case 0xe8:
					case 0xe9:
					case 0xea:
					case 0xeb:
					case 0xec:
					case 0xed:
					case 0xee:
					case 0xef:
					case 0xf0:
					case 0xfd:
					case 0xfe:
						state = STATE_LENHI;
						break;

					default:
						state = STATE_NONE;
						break;
				}
				break;

			case STATE_LENHI:
				length = byte << 8;
				state  = STATE_LENLO;
				break;

			case STATE_LENLO:
				length |= byte;

				switch (length) {
					case 0:
					case 1:
						app   = 0;
						state = STATE_NONE;
						break;

					case 2:
						state = STATE_NONE;
						break;

					default:
						length -= 2;
						if (app) {
							state = (length < 14) ? STATE_NONE : app;
							app   = 0;
						} else {
							state = STATE_SKIPPING;
						}
						break;
				}
				break;

			case STATE_APP00:
				state = (byte == 0x4a) ? STATE_APP01 : STATE_NONE;
				break;

			case STATE_APP01:
				state = (byte == 0x46) ? STATE_APP02 : STATE_NONE;
				break;

			case STATE_APP02:
				state = (byte == 0x49) ? STATE_APP03 : STATE_NONE;
				break;

			case STATE_APP03:
				state = (byte == 0x46) ? STATE_APP04 : STATE_NONE;
				break;

			case STATE_APP04:
			case STATE_APP14:
				if (byte == 0x00) {
					ofd     = writefile(length, state);
					length -= 5;
					start   = count;
					state   = STATE_SKIPPING;
				} else {
					state   = STATE_NONE;
				}
				break;

			case STATE_APP10:
				state = (byte == 0x45) ? STATE_APP11 : STATE_NONE;
				break;

			case STATE_APP11:
				state = (byte == 0x78) ? STATE_APP12 : STATE_NONE;
				break;

			case STATE_APP12:
				state = (byte == 0x69) ? STATE_APP13 : STATE_NONE;
				break;

			case STATE_APP13:
				state = (byte == 0x66) ? STATE_APP14 : STATE_NONE;
				break;

			case STATE_SKIPPING:
				if (--length == 0) {
					if (image) {
						state = STATE_IMAGE;
						image = FALSE;
					} else {
						state = STATE_NONE;
					}
				}
				break;

			case STATE_IMAGE:
				if (byte == MARKER) {
					state = STATE_IMARKER;
				}
				break;

			case STATE_IMARKER:
				switch (byte) {
					case 0xd9:
						if (ofd >= 0) {
							printf("%d bytes\n",
								count - start);
							close(ofd);
							ofd   = -1;
							state = STATE_NONE;
						}
						break;

					case MARKER:
						break;

					default:
						state = STATE_IMAGE;
						break;
				}
				break;
		}
	}
}

/* open new file for writing and regenerate header */

static int
writefile(
	int		length,				/* remaining length of APP0 field */
	int		type)				/* JFIF or Exif */
{
	char	outfile[FILENAME];	/* output file name */
	int		ofd;				/* output file descriptor */

	sprintf(outfile, Outfile, fnum++);

	ofd = open(outfile, O_WRONLY | O_CREAT | O_EXCL, 0666);

	if (ofd < 0) {
		whyerror(outfile);
	}

	printf("saving to %s\n", outfile);

	length += 2;	/* restore length bytes */

	putbyte(ofd, MARKER,       0);
	putbyte(ofd, 0xd8,         1);	/* SOI */
	putbyte(ofd, MARKER,       2);
	putbyte(ofd, type,         3);	/* APP0 or APP1 */
	putbyte(ofd, length >> 8,  4);
	putbyte(ofd, length,       5);

	if (type == STATE_APP04) {
		putbyte(ofd, 0x4a,         6);	/* 'J' */
		putbyte(ofd, 0x46,         7);	/* 'F' */
		putbyte(ofd, 0x49,         8);	/* 'I' */
		putbyte(ofd, 0x46,         9);	/* 'F' */
	} else {
		putbyte(ofd, 0x45,         6);	/* 'E' */
		putbyte(ofd, 0x78,         7);	/* 'x' */
		putbyte(ofd, 0x69,         8);	/* 'i' */
		putbyte(ofd, 0x66,         9);	/* 'f' */
	}

	putbyte(ofd, 0x00,        10);	/* '\0' */

	return(ofd);
}

/* write a byte to a file */

static void
putbyte(
	int		fd,		/* file descriptor */
	int		value,	/* value of byte to write */
	int		pos)	/* position */
{
	char	byte;	/* byte value */

	byte = value;

	if (write(fd, &byte, sizeof(byte)) != sizeof(byte)) {
		error("can't write byte %d", pos);
	}
}

/* set currently running program name */

static void
setprog(
	char *	name)	/* program name to use */
{
	prog = strrchr(name, PATHSEP);

	if (prog) {
		prog++;
	} else {
		prog = name;
	}
}

/* print out message and exit */

static void
error(
	const char *	fmt,	/* message format */
					...)	/* format arguments */
{
	va_list			ap;		/* argument pointer */

	fflush(stdout);

	fprintf(stderr, "%s: ", prog);

	va_start(ap, fmt);

	vfprintf(stderr, fmt, ap);

	va_end(ap);

	fprintf(stderr, "\n");

	exit(EXIT_FAILURE);
}

/* print out error message and reason and exit */

static void
whyerror(
	const char *	fmt,	/* message format */
					...)	/* format arguments */
{
	char *			errstr;	/* error string pointer */
	va_list			ap;		/* argument pointer */

	fflush(stdout);

	fprintf(stderr, "%s: ", prog);

	va_start(ap, fmt);

	vfprintf(stderr, fmt, ap);

	va_end(ap);

	errstr = strerror(errno);

	if (errstr) {
		fprintf(stderr, ": %s\n", errstr);
	} else {
		fprintf(stderr, ": errno=%d\n", errno);
	}

	if (errno) {
		exit(errno);
	}

	exit(EXIT_FAILURE);
}

/* allocate a buffer and punt on failure */

static void *
mkbuf(
	int		size)		/* size of buffer to allocate */
{
	void *	ptr;		/* address of new buffer */

	ptr = malloc(size);

	if (ptr == NULL) {
		whyerror("Malloc(%d)", size);
	}

	return(ptr);
}
