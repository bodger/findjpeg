# findjpeg

Look for JPEG formatted files in raw data.

I originally wrote this in 2005 when a friend's laptop drive failed right after his first child was born, and he hadn't had a chance to back up the baby pictures.  Due to a quirk of his camera, the simple software of the time gave up because one of the fields just happened to have the wrong combination of byte values.
Happily, the code was able to recover about 3/4 of the pictures.

Note that in a dump from a heavily fragmented drive (or card), it won't work well, as it expects to find the files in order, but it cares nothing for filesystem structure otherwise.

There are some decent free JPEG extractors out there now (File Juicer for one),
but I'm putting this up in case it's useful to anyone (if only for parsing JPEG headers).
